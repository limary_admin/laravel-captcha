<?php
namespace Sinta\Laravel\Captcha;

use Exception;
use Illuminate\Config\Repository;
use Illuminate\Hashing\BcryptHasher as Hasher;
use Illuminate\Filesystem\Filesystem;
use Intervention\Image\ImageManager;
use Illuminate\Support\Str;
use Illuminate\Session\Store as Session;

class Captcha
{
    /**
     * 文件系统
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * 配置资源
     *
     * @var Repository
     */
    protected $config;


    /**
     * 图片管理器
     *
     * @var ImageManager
     */
    protected $imageManager;

    /**
     * 会话
     *
     * @var Session
     */
    protected $session;


    /**
     * @var Hasher
     */
    protected $hasher;


    protected $canvas;

    protected $image;

    /**
     * @var array
     */
    protected $backgrounds = [];


    protected $fontColors = [];

    //长度
    protected $length = 5;

    //宽度
    protected $width = 120;

    //高度
    protected $height = 36;

    //所用到的字符
    protected $characters;

    protected $text;

    protected $contrast = 0;

    protected $quality = 90;


    protected $sharpen = 0;


    protected $blur = 0;


    protected $bgImage = true;

    /**
     * 景色
     *
     * @var string
     */
    protected $bgColor = '#ffffff';


    protected $invert = false;


    protected $sensitive = false;


    public function __construct(Filesystem $files,
                                Repository $config,
                                ImageManager $imageManager,
                                Session $session,
                                Hasher $hasher,
                                Str $str)
    {
        $this->files = $files;
        $this->config = $config;
        $this->imageManager = $imageManager;
        $this->hasher = $hasher;
        $this->str = $str;
        $this->characters = config('captcha.characters','2346789abcdefghjmnpqrtuxyzABCDEFGHJMNPQRTUXYZ');
    }


    protected function configure($config)
    {
        if ($this->config->has('captcha.' . $config)){
            foreach($this->config->get('captcha.'.$config) as $key => $val){
                $this->{$key} = $val;
            }
        }
    }


    public function create($config = 'default')
    {
        $this->backgrounds = $this->files->files(__DIR__.'/../assets/backgrounds');
        $this->fonts = $this->files->files(__DIR__."/../assets/fonts");

        if (app()->version() >= 5.5){
            $this->fonts = array_map(function($file) {
                return $file->getPathName();
            }, $this->fonts);
        }

        $this->fonts = array_values($this->fonts);
        $this->configure($config);
        $this->canvas = $this->imageManager->canvas(
            $this->width,
            $this->height,
            $this->bgColor
        );

        if($this->bgImage){//如果有背景图片
            $this->image = $this->imageManager->make($this->background())->resize($this->width,$this->height);
            $this->canvas->insert($this->image);
        }else{
            $this->image = $this->canvas;
        }

        if($this->contrast != 0){
            $this->image->contrast($this->contrast);
        }

        $this->text();

        $this->lines();

        if($this->sharpen){
            $this->image->sharpen($this->sharpen);
        }

        if($this->invert){
            $this->image->invert($this->invert);
        }

        if($this->blur){
            $this->image->blur($this->blur);
        }

        return $this->image->response('png',$this->quality);
    }


    protected function background()
    {
        return $this->backgrounds[rand(0, count($this->backgrounds) - 1)];
    }

    protected function generate()
    {
        $characters = str_split($this->characters);
        $bag = '';
        for($i = 0; $i < $this->length; $i++)
        {
            $bag .= $characters[rand(0, count($characters) - 1)];
        }
        $this->session->put('captcha', [
            'sensitive' => $this->sensitive,
            'key'       => $this->hasher->make($this->sensitive ? $bag : $this->str->lower($bag))
        ]);
        return $bag;
    }


    protected function text()
    {
        $marginTop = $this->image->height() / $this->length;
        $i = 0;
        foreach(str_split($this->text) as $char)
        {
            $marginLeft = ($i * $this->image->width() / $this->length);
            $this->image->text($char, $marginLeft, $marginTop, function($font) {
                $font->file($this->font());
                $font->size($this->fontSize());
                $font->color($this->fontColor());
                $font->align('left');
                $font->valign('top');
                $font->angle($this->angle());
            });
            $i++;
        }
    }

    protected function font()
    {
        return $this->fonts[rand(0, count($this->fonts) - 1)];
    }


    protected function fontSize()
    {
        return rand($this->image->height() - 10, $this->image->height());
    }


    protected function fontColor()
    {
        if ( ! empty($this->fontColors))
        {
            $color = $this->fontColors[rand(0, count($this->fontColors) - 1)];
        }
        else
        {
            $color = [rand(0, 255), rand(0, 255), rand(0, 255)];
        }
        return $color;
    }

    protected function angle()
    {
        return rand((-1 * $this->angle), $this->angle);
    }


    protected function lines()
    {
        for($i = 0; $i <= $this->lines; $i++)
        {
            $this->image->line(
                rand(0, $this->image->width()) + $i * rand(0, $this->image->height()),
                rand(0, $this->image->height()),
                rand(0, $this->image->width()),
                rand(0, $this->image->height()),
                function ($draw) {
                    $draw->color($this->fontColor());
                }
            );
        }
        return $this->image;
    }

    public function check($value)
    {
        if ( ! $this->session->has('captcha'))
        {
            return false;
        }
        $key = $this->session->get('captcha.key');
        if ( ! $this->session->get('captcha.sensitive'))
        {
            $value = $this->str->lower($value);
        }
        $this->session->remove('captcha');
        return $this->hasher->check($value, $key);
    }

    /**
     * 图片源
     *
     * @param null $config
     * @return string
     */
    public function src($config = null)
    {
        return url('captcha' . ($config ? '/' . $config : '/default')) . '?' . $this->str->random(8);
    }

    /**
     * 产生图片
     *
     * @param null $config
     * @return string
     */
    public function img($config = null)
    {
        return '<img src="' . $this->src($config) . '" alt="captcha">';
    }
}