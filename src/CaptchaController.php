<?php
namespace Sinta\Laravel\Captcha;

use Illuminate\Routing\Controller;

class CaptchaController extends Controller
{
    public function getCaptcha(Captcha $captcha, $config = 'default')
    {
        return $captcha->create($config);
    }
}