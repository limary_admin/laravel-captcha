<?php
namespace Sinta\Laravel\Captcha;

use Illuminate\Support\ServiceProvider;


class CaptchaServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/captcha.php' =>config_path('captcha.php')
        ],'config');

        if((double) $this->app->version() >= 5.2){
            $this->app['router']->get('captcha/{config?}',"\Sinta\Laravel\Captcha\CaptchaController@getCaptcha")->middleware('web');
        }else{
            $this->app['router']->get('captcha/{config?}', '\Mews\Captcha\CaptchaController@getCaptcha');
        }

        //验证
        $this->app['validator']->extend('captcha', function($attribute, $value, $parameters)
        {
            return captcha_check($value);
        });
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/captcha.php', 'captcha');

        $this->app->bind('captcha', function($app) {
            return new Captcha(
                $app['Illuminate\Filesystem\Filesystem'],
                $app['Illuminate\Config\Repository'],
                $app['Intervention\Image\ImageManager'],
                $app['Illuminate\Session\Store'],
                $app['Illuminate\Hashing\BcryptHasher'],
                $app['Illuminate\Support\Str']
            );
        });
    }
}