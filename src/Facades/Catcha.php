<?php
namespace Sinta\Laravel\Captcha\Facades;


use Illuminate\Support\Facades\Facade;

class Catcha extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'catcha';
    }
}