<?php
if(! function_exists('captcha')){
    /**
     * 验证码
     *
     * @param string $config
     * @return mixed
     */
    function captcha($config = 'default')
    {
        return app('captcha')->create($config);
    }
}

if(!function_exists('captcha_src')){
    /**
     *图片src
     *
     * @param string $config
     * @return mixed
     */
    function captcha_src($config = 'default')
    {
        return app('captcha')->src($config);
    }
}


if ( ! function_exists('captcha_img')) {
    /**
     * 验证码图片
     *
     * @param string $config
     * @return mixed
     */
    function captcha_img($config = 'default')
    {
        return app('captcha')->img($config);
    }
}


if ( ! function_exists('captcha_check')) {

    /**
     *验证码检测
     *
     *
     * @param $value
     * @return boolean
     */
    function captcha_check($value)
    {
        return app('captcha')->check($value);
    }
}